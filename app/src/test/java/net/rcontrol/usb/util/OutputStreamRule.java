package net.rcontrol.usb.util;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class OutputStreamRule implements TestRule {

    private final ByteArrayOutputStream out = new ByteArrayOutputStream();
    private final ByteArrayOutputStream err = new ByteArrayOutputStream();

    @Override
    public Statement apply(final Statement statement, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                System.setOut(new PrintStream(out));
                System.setErr(new PrintStream(err));
                statement.evaluate();
            }
        };
    }

    public String getOut() {
        return out.toString();
    }

    public String getErr() {
        return err.toString();
    }
}
