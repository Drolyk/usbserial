package net.rcontrol.usb.util;

import junit.framework.Assert;

import org.junit.Rule;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.Charset;

public class BytesInputStreamTest {

    @Rule
    public final OutputStreamRule outputStreamRule = new OutputStreamRule();

    @Test
    public void ruleTest() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello").append("\n");
        sb.append("hello").append("\n");
        System.out.print(sb.toString());
        Assert.assertEquals(sb.toString(), outputStreamRule.getOut());
    }

    @Test(timeout = 10000)
    public void readWriteTest() throws InterruptedException {
        final BytesInputStream in = new BytesInputStream();
        final StringBuffer sb = new StringBuffer();

        StoppableThread reader = new StoppableThread() {
            @Override
            public boolean runImpl() {
                try {
                    int r;
                    while ((r = in.read()) != -1) {
                        System.out.write(r);
                    }
                } catch (IOException e) {
                    //Ignore here, just exit
                }
                return false;
            }
        };
        StoppableThread writer = new StoppableThread() {
            private int count;

            @Override
            public boolean runImpl() {
                String s = "String " + count + "\n";
                sb.append(s);
                in.put(s.getBytes(Charset.defaultCharset()));
                try {
                    Thread.sleep(1 + (int) (Math.random() * 30));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    count = 100;
                }
                return count++ < 50;
            }
        };
        writer.start();
        Thread.sleep(1000);
        reader.start();
        Thread.sleep(1000);
        reader.setRunning(false);
        writer.setRunning(false);
        Thread.sleep(1000);
        reader.interrupt();
        writer.interrupt();

        Assert.assertEquals(sb.toString(), outputStreamRule.getOut());
    }

    private abstract static class StoppableThread extends Thread {
        private volatile boolean running = true;

        public abstract boolean runImpl();


        @Override
        public void run() {
            while (running && runImpl()) ;
        }

        public void setRunning(boolean running) {
            this.running = running;
        }
    }
}
