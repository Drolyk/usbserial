package net.rcontrol.usb;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.squareup.otto.Bus;

import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.event.UsbDataEvent;
import net.rcontrol.usb.event.UsbErrorEvent;
import net.rcontrol.usb.event.UsbPlugEvent;
import net.rcontrol.usb.event.UsbRefreshListEvent;
import net.rcontrol.usb.util.AppUtils;
import net.rcontrol.usb.util.service.AppStateService;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;

public class UsbService extends Service {

    public static final int REFRESH_DELAY_TIMEOUT = 1000;

    private static final String TAG = UsbService.class.getSimpleName();
    private static final String ACTION_USB_PERMISSION = "net.rcontrol.usb.ACTION_USB_PERMISSION";

    private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
    private final IBinder mBinder = new LocalBinder();
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.i(TAG, "Broadcast action: " + action);

            if (TextUtils.equals(UsbManager.ACTION_USB_DEVICE_ATTACHED, action)) {
                UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                mEventBus.post(new UsbPlugEvent(usbDevice, true));
                Log.i(TAG, "Device atached: " + usbDevice.getDeviceName());
                return;
            }

            if (TextUtils.equals(UsbManager.ACTION_USB_DEVICE_DETACHED, action)) {
                UsbDevice usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                mUsbSerialPorts = null;
                stop();
                Log.i(TAG, "Device detached: " + usbDevice.getDeviceName());
                mEventBus.post(new UsbPlugEvent(usbDevice, false));
                return;
            }

            if (ACTION_USB_PERMISSION.equals(action)) {
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    if (device != null) {
                        UsbSerialDriver driver = UsbSerialProber.getDefaultProber().probeDevice(device);
                        if (driver == null) {
                            mEventBus.post(new AppStateEvent(null));
                        } else {
                            start(mSelectedPort == null ? driver.getPorts().get(0) : mSelectedPort);
                        }
                        mPermissionRequestPending = false;
                    }
                } else {
                    Log.d(TAG, "permission denied for device " + device);
                }

            }
        }
    };

    private final SerialInputOutputManager.Listener mListener = new SerialInputOutputManager.Listener() {
        @Override
        public void onRunError(Exception e) {
            Log.d(TAG, "Runner stopped.");
        }

        @Override
        public void onNewData(final byte[] data) {
            mEventBus.post(new UsbDataEvent(data));
        }
    };

    private AppStateService mAppStateService;
    private PendingIntent mPermissionIntent;
    private boolean mPermissionRequestPending;
    private UsbSerialPort mSelectedPort;
    private SerialInputOutputManager mSerialIoManager;
    private UsbManager mUsbManager;
    private List<UsbSerialPort> mUsbSerialPorts;
    private volatile boolean mRefresh;

    @Inject
    Bus mEventBus;

    @Override
    public void onCreate() {
        MainApp.get(this).getComponent().inject(this);
        mAppStateService = new AppStateService();
        mAppStateService.register(this);
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        registerReceiver(mUsbReceiver, new IntentFilter(ACTION_USB_PERMISSION));
        registerReceiver(mUsbReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED));
        registerReceiver(mUsbReceiver, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mUsbReceiver);
        stop();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void refreshDeviceList() {
        if (mRefresh) {
            return;
        }
        new AsyncTask<Void, Void, List<UsbSerialPort>>() {
            @Override
            protected List<UsbSerialPort> doInBackground(Void... params) {
                mRefresh = true;
                Log.d(TAG, "Refreshing device list ...");
                SystemClock.sleep(REFRESH_DELAY_TIMEOUT);

                List<UsbSerialDriver> drivers = UsbSerialProber.getDefaultProber().findAllDrivers(mUsbManager);

                List<UsbSerialPort> result = new ArrayList<>();
                for (final UsbSerialDriver driver : drivers) {
                    final List<UsbSerialPort> ports = driver.getPorts();
                    Log.d(TAG, String.format("+ %s: %s port%s", driver, ports.size(), ports.size() == 1 ? "" : "s"));
                    result.addAll(ports);
                }

                return result;
            }

            @Override
            protected void onPostExecute(List<UsbSerialPort> usbSerialPorts) {
                mUsbSerialPorts = usbSerialPorts;
                mRefresh = false;
                mEventBus.post(new UsbRefreshListEvent());
            }
        }.execute((Void) null);
    }

    public class LocalBinder extends Binder {
        public UsbService get() {
            return UsbService.this;
        }
    }

    public boolean isActive(UsbDevice usbDevice) {
        return mSelectedPort != null && mSelectedPort.getDriver().getDevice().equals(usbDevice);
    }

    public boolean isActive() {
        return mSelectedPort != null;
    }

    public boolean isPermissionRequestPending() {
        return mPermissionRequestPending;
    }

    public void stop() {
        Log.i(TAG, "Stopping io manager ..");
        stopIoManager();
        AppUtils.closeQuietly(mSelectedPort);
        mSelectedPort = null;
        mAppStateService.setExtraTitle(null);
    }

    public void start(UsbSerialPort usbPort) {
        Log.i(TAG, "Starting io manager ..");
        mSelectedPort = usbPort;
        UsbDevice device = usbPort.getDriver().getDevice();
        if (!mUsbManager.hasPermission(device)) {
            if (!mPermissionRequestPending) {
                mUsbManager.requestPermission(device, mPermissionIntent);
                mPermissionRequestPending = true;
            }
            return;
        }
        UsbDeviceConnection connection = mUsbManager.openDevice(device);
        if (connection == null) {
            Log.e(TAG, "Error getting connection from usb device: " + mSelectedPort.getDriver().getDevice().getDeviceName());
            mEventBus.post(new UsbErrorEvent(getString(R.string.usb_error_connection)));
            return;
        }
        try {
            Map<String, Integer> config = AppUtils.createSerialContext(getApplicationContext());
            mSelectedPort.open(connection);
            mSelectedPort.setParameters(config.get(AppUtils.BAUD_RATE),
                    config.get(AppUtils.DATA_BITS),
                    config.get(AppUtils.STOP_BITS),
                    config.get(AppUtils.PARITY));
        } catch (IOException e) {
            Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
            mEventBus.post(new UsbErrorEvent(e.getMessage()));
            AppUtils.closeQuietly(mSelectedPort);
            mSelectedPort = null;
            return;
        }
        onDeviceStateChange();
        mAppStateService.setExtraTitle(createExtraTitle(device));
    }

    public void write(byte[] data) {
        if (mSerialIoManager != null) {
            Log.i(TAG, "writeAsync(byte[]) byte[].size: " + data.length);
            mSerialIoManager.writeAsync(data);
        }
    }

    public void write(String msg) {
        if (mSerialIoManager != null) {
            byte[] data = msg.getBytes(Charset.forName("UTF-8"));
            Log.i(TAG, "writeAsync(String) byte[].size: " + data.length);
            mSerialIoManager.writeAsync(data);
        }
    }

    public List<UsbSerialPort> getUsbSerialPorts() {
        return mUsbSerialPorts == null ? new ArrayList<UsbSerialPort>() : mUsbSerialPorts;
    }

    public String createExtraTitle(UsbDevice device) {
        return String.format(getString(R.string.usb_list_item),
                HexDump.toHexString((short) device.getVendorId()),
                HexDump.toHexString((short) device.getProductId()));
    }

    private void onDeviceStateChange() {
        stopIoManager();
        startIoManager();
    }

    private void stopIoManager() {
        if (mSerialIoManager != null) {
            Log.i(TAG, "Stopping io manager ..");
            mSerialIoManager.stop();
            mSerialIoManager = null;
        }
    }

    private void startIoManager() {
        if (mSelectedPort != null) {
            Log.i(TAG, "Starting io manager ..");
            mSerialIoManager = new SerialInputOutputManager(mSelectedPort, mListener);
            mExecutor.submit(mSerialIoManager);
        }
    }
}
