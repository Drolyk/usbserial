package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.usb.rcontrol.net.usbserial.R;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import jackpal.androidterm.emulatorview.EmulatorView;

public class ControlFragment extends AbstractFragment {

    private Button mSendButton;
    private EditText mInputTextView;
    private EmulatorView mEmulatorView;
    private ControlCoordinator mCoordinator;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.content_control, container, false);
        mSendButton = (Button) root.findViewById(R.id.sendButton);
        mInputTextView = (EditText) root.findViewById(R.id.inputTextView);
        mEmulatorView = (EmulatorView) root.findViewById(R.id.emulatorView);

        mCoordinator = new ControlCoordinator();
        mCoordinator.init(this);

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_control, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_disconnect:
                mCoordinator.disconnect();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCoordinator.cleanUp();
    }

    @Override
    public Type getType() {
        return Type.CONTROL;
    }

    @Override
    public void setupFragmentComponent() {
        mCoordinator.register();
    }

    @Override
    public void shutdownFragmentComponent() {
        mCoordinator.unregister();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mCoordinator.storeState(outState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        mCoordinator.restoreState(savedInstanceState);
    }

    public Button getSendButton() {
        return mSendButton;
    }

    public EditText getInputTextView() {
        return mInputTextView;
    }

    public EmulatorView getEmulatorView() {
        return mEmulatorView;
    }
}
