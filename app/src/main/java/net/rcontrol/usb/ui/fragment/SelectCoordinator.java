package net.rcontrol.usb.ui.fragment;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.util.HexDump;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.UsbService;
import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.event.UsbServiceEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class SelectCoordinator extends FragmentCoordinator<SelectFragment> {

    private static final String TAG = SelectCoordinator.class.getSimpleName();

    private final Handler mHandler = new Handler();

    private UsbService mUsbService;

    @Inject
    Bus mEventBus;

    @Inject
    Map<String, Serializable> mAppState;

    @Inject
    MainApp mApp;

    @Override
    public void init(SelectFragment fragment) {
        super.init(fragment);
        FragmentActivity activity = fragment.getActivity();
        MainApp.get(activity).getComponent().inject(this);
        fragment.setHasOptionsMenu(true);
    }

    @Override
    public void register() {
        mEventBus.register(this);
        if (mApp.isUsbServiceReady()) {
            mUsbService = mApp.getUsbService();
            initUi();
        }
    }

    @Override
    public void unregister() {
        mEventBus.unregister(this);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Subscribe
    public void handleUsbService(UsbServiceEvent e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbService");
                mUsbService = mApp.getUsbService();
                initUi();
            }
        });
    }

    public void refresh() {
        mEventBus.post(new AppStateEvent(null));
    }

    private void initUi() {
        final SelectFragment fragment = getRef();
        final FragmentActivity activity = fragment.getActivity();
        final List<UsbSerialPort> usbSerialPorts = mUsbService.getUsbSerialPorts();

        ArrayAdapter<UsbSerialPort> arrayAdapter = new ArrayAdapter<UsbSerialPort>(activity,
                android.R.layout.simple_expandable_list_item_2, usbSerialPorts) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                final View row;
                if (convertView == null) {
                    final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    row = inflater.inflate(R.layout.spinner_item, null);
                } else {
                    row = convertView;
                }

                UsbSerialPort port = usbSerialPorts.get(position);
                UsbSerialDriver driver = port.getDriver();
                UsbDevice device = driver.getDevice();

                TextView deviceName = (TextView) row.findViewById(R.id.deviceName);
                deviceName.setText(driver.getClass().getSimpleName());

                TextView deviceInfo = (TextView) row.findViewById(R.id.deviceInfo);
                deviceInfo.setText(createInfoText(device));

                return row;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                return getView(position, convertView, parent);
            }
        };

        final Spinner usbPortSpinner = fragment.getUsbPortSpinner();
        final Button selectButton = fragment.getSelectButton();

        usbPortSpinner.setAdapter(arrayAdapter);
        usbPortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectButton.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                selectButton.setEnabled(false);
            }
        });

        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUsbService != null && !mUsbService.isPermissionRequestPending()) {
                    int idx = usbPortSpinner.getSelectedItemPosition();
                    mUsbService.start(usbSerialPorts.get(idx));
                    mEventBus.post(new AppStateEvent(fragment.getType()));
                }
            }
        });
    }

    private String createInfoText(UsbDevice device) {
        SelectFragment fragment = getRef();
        return String.format(fragment.getString(R.string.usb_list_item),
                HexDump.toHexString((short) device.getVendorId()),
                HexDump.toHexString((short) device.getProductId()));
    }
}
