package net.rcontrol.usb.ui.fragment;

import android.support.v4.app.Fragment;

public abstract class AbstractFragment extends Fragment implements TypedFragment {

    @Override
    public void onResume() {
        super.onResume();
        setupFragmentComponent();
    }

    @Override
    public void onPause() {
        super.onPause();
        shutdownFragmentComponent();
    }

    @Override
    public void beforeNextType() {
    }
}
