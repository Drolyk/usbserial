package net.rcontrol.usb.ui.fragment;

public interface TypedFragment {
    String FM_MAIN_TAG = "fmMainTag";

    Type getType();

    void beforeNextType();

    void setupFragmentComponent();

    void shutdownFragmentComponent();
}
