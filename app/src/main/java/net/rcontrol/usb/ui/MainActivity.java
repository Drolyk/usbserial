package net.rcontrol.usb.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.usb.rcontrol.net.usbserial.R;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import net.rcontrol.usb.ui.fragment.Type;
import net.rcontrol.usb.util.service.UiService;

public class MainActivity extends AbstractActivity {

    private static final int EXIT_ASK_TIMEOUT = 3000;

    private boolean exitAsk = false;
    private Toolbar mToolbar;
    private MainCoordinator mCoordinator;
    private UiService mUiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCoordinator.register();
        mCoordinator.initUi(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings_action:
                mCoordinator.requestFragment(Type.SETTINGS);
                return true;
            case R.id.exit_action:
                actionExit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setupActivityComponent() {
        mCoordinator = new MainCoordinator();
        mCoordinator.init(this);
        mUiService = new UiService();
        mUiService.register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCoordinator.unregister();
    }

    @Override
    protected void actionExit() {
        mCoordinator.stopUsb();
        super.actionExit();
    }

    @Override
    public void onBackPressed() {
        if (mCoordinator.backNavigationSupport()) {
            return;
        }
        if (exitAsk) {
            super.onBackPressed();
            actionExit();
            return;
        }
        mUiService.createToast(UiService.ToastState.OK, R.string.exit_ask);
        exitAsk = true;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                exitAsk = false;
            }
        }, EXIT_ASK_TIMEOUT);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}