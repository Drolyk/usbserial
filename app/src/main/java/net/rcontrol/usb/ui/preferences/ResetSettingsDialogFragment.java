package net.rcontrol.usb.ui.preferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.DialogPreference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.support.v7.preference.PreferenceManager;

public class ResetSettingsDialogFragment extends PreferenceDialogFragmentCompat {

    private ResetSettingsPreference mPreference;

    public static ResetSettingsDialogFragment newInstance(String key) {
        ResetSettingsDialogFragment fragment = new ResetSettingsDialogFragment();
        Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DialogPreference.TargetFragment fragment = (DialogPreference.TargetFragment) this.getTargetFragment();
        String key = this.getArguments().getString("key");
        this.mPreference = (ResetSettingsPreference) fragment.findPreference(key);
    }

    @Override
    public void onDialogClosed(boolean result) {
        if (result) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            preferences.edit()
                    .clear()
                    .commit();
            mPreference.persist();
        }
    }
}
