package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.usb.rcontrol.net.usbserial.R;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

public class SelectFragment extends AbstractFragment {

    private Spinner mUsbPortSpinner;
    private Button mSelectButton;
    private SelectCoordinator mCoordinator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.content_select, container, false);
        mUsbPortSpinner = (Spinner) root.findViewById(R.id.usbPortSpinner);
        mSelectButton = (Button) root.findViewById(R.id.selectButton);
        mCoordinator = new SelectCoordinator();
        mCoordinator.init(this);

        return root;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void setupFragmentComponent() {
        mCoordinator.register();
    }

    @Override
    public void shutdownFragmentComponent() {
        mCoordinator.unregister();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_select, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mCoordinator.refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Type getType() {
        return Type.SELECT;
    }

    public Spinner getUsbPortSpinner() {
        return mUsbPortSpinner;
    }

    public Button getSelectButton() {
        return mSelectButton;
    }
}
