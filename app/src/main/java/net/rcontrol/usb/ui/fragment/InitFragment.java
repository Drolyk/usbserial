package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.usb.rcontrol.net.usbserial.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

public class InitFragment extends AbstractFragment {

    private TextView mStatusTextView;
    private ProgressBar mProgress;
    private InitCoordinator mCoordinator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.content_init, container, false);
        mStatusTextView = (TextView) root.findViewById(R.id.initStatusMessage);
        mProgress = (ProgressBar) root.findViewById(R.id.initProgressBar);
        mCoordinator = new InitCoordinator();
        mCoordinator.init(this);
        return root;
    }

    @Override
    public Type getType() {
        return Type.INIT;
    }

    @Override
    public void setupFragmentComponent() {
        mCoordinator.register();
    }

    @Override
    public void shutdownFragmentComponent() {
        mCoordinator.unregister();
    }

    public TextView getStatusTextView() {
        return mStatusTextView;
    }

    public ProgressBar getProgress() {
        return mProgress;
    }
}
