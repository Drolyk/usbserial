package net.rcontrol.usb.ui;

import android.hardware.usb.UsbDevice;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;
import android.view.View;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.rcontrol.usb.Coordinator;
import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.UsbService;
import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.event.UsbErrorEvent;
import net.rcontrol.usb.event.UsbPlugEvent;
import net.rcontrol.usb.event.UsbServiceEvent;
import net.rcontrol.usb.ui.fragment.Type;
import net.rcontrol.usb.ui.fragment.TypedFragment;
import net.rcontrol.usb.util.service.AppStateService;
import net.rcontrol.usb.util.service.UiService;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

public class MainCoordinator extends Coordinator<MainActivity> {

    private static final String TAG = MainCoordinator.class.getSimpleName();
    private static final String USB_EVENT_KEY = "mainActivity.usbEventKey";

    private final Handler mHandler = new Handler();

    private AppStateService mAppStateService;
    private UiService mUiService;
    private UsbService mUsbService;

    @Inject
    MainApp mApp;
    @Inject
    Map<String, Serializable> mAppState;
    @Inject
    Bus mEventBus;

    @Override
    public void init(MainActivity activity) {
        super.init(activity);
        MainApp.get(activity).getComponent().inject(this);
    }

    @Override
    public void register() {
        MainActivity activity = getRef();
        mEventBus.register(this);
        mAppStateService = new AppStateService();
        mAppStateService.register(activity);
        mUiService = new UiService();
        mUiService.register(activity);
        if (mApp.isUsbServiceReady()) {
            initUsbService();
        }
    }

    @Override
    public void unregister() {
        mEventBus.unregister(this);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Subscribe
    public void handleUsbError(UsbErrorEvent e) {
        final String msg = e.getErrorMessage();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbError: " + msg);
                Map<String, Serializable> ctx = new HashMap<>();
                ctx.put(USB_EVENT_KEY, true);
                mUiService.createToast(UiService.ToastState.ERROR, msg);
                requestFragment(Type.INIT, ctx);
            }
        });
    }

    @Subscribe
    public void handleUsbUpdate(UsbPlugEvent e) {
        final boolean attached = e.isAttached();
        final UsbDevice usbDevice = e.getUsbDevice();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbUpdate: " + usbDevice.getDeviceName() + ", attached: " + attached);
                Map<String, Serializable> ctx = new HashMap<>();
                ctx.put(USB_EVENT_KEY, true);
                if (attached) {
                    if (!mUsbService.isActive()) {
                        requestFragment(Type.INIT, ctx);
                    }
                    return;
                }

                if (mUsbService.isActive(usbDevice)) {
                    mUsbService.stop();
                    requestFragment(Type.INIT, ctx);
                    return;
                }

                requestFragment(Type.INIT, ctx);
            }
        });
    }

    @Subscribe
    public void handleAppState(final AppStateEvent e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleAppState: " + e.getType());
                Type t = e.getType();
                requestFragment(t == null ? Type.INIT : t.getNext(mAppState), e.getCtx());
            }
        });
    }

    @Subscribe
    public void handleUsbService(UsbServiceEvent e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbService");
                initUsbService();
            }
        });
    }

    public void initUi(Bundle savedInstanceState) {
        MainActivity activity = getRef();
        Toolbar toolbar = activity.getToolbar();
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backNavigationSupport();
            }
        });
        Type fmType = mAppStateService.getFragmentType();
        if (savedInstanceState == null) {
            requestFragment(fmType);
        }
        setDisplayHomeAsUpEnabled(fmType);
    }

    public void stopUsb() {
        if (mUsbService != null && mUsbService.isActive()) {
            mUsbService.stop();
        }
    }

    public void requestFragment(Type type) {
        requestFragment(type, Collections.<String, Serializable>emptyMap());
    }

    public void requestFragment(Type type, Map<String, Serializable> ctx) {
        MainActivity activity = getRef();
        if (activity.isFinishing()) {
            Log.e(TAG, "Activity finishing. Skip viewFragment transition");
            return;
        }
        if (ctx.containsKey(USB_EVENT_KEY)) {
            Type currentType = mAppStateService.getFragmentType();
            if (!currentType.isUsbAware()) {
                return;
            }
        }
        activity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.main_container, type.createFragment(ctx), TypedFragment.FM_MAIN_TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
        setDisplayHomeAsUpEnabled(type);
        mAppStateService.setFragmentType(type);
        activity.getToolbar().setSubtitle(mAppStateService.getExtraTitle());
    }

    private void setDisplayHomeAsUpEnabled(Type type) {
        MainActivity activity = getRef();
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            boolean enabled = getBackType(type) != null;
            actionBar.setHomeButtonEnabled(enabled);
            actionBar.setDisplayHomeAsUpEnabled(enabled);
        }
    }

    public boolean backNavigationSupport() {
        MainActivity activity = getRef();
        Type backType = getBackType(mAppStateService.getFragmentType());
        if (backType != null) {
            TypedFragment typedFragment = (TypedFragment) activity.getSupportFragmentManager()
                    .findFragmentByTag(TypedFragment.FM_MAIN_TAG);
            typedFragment.beforeNextType();
            requestFragment(backType);
        }
        return backType != null;
    }

    private Type getBackType(Type type) {
        mAppState.put(AppStateService.USB_SERVICE_ACTIVE, mUsbService != null && mUsbService.isActive());
        return type.getBack(mAppState);
    }

    private void initUsbService() {
        mUsbService = mApp.getUsbService();
        Type fmType = mAppStateService.getFragmentType();
        if (!mUsbService.isActive() && fmType.isUsbAware()) {
            requestFragment(Type.INIT);
        }
    }
}
