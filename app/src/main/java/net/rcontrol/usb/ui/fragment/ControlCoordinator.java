package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.method.TextKeyListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.UsbService;
import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.event.UsbDataEvent;
import net.rcontrol.usb.event.UsbServiceEvent;
import net.rcontrol.usb.util.AppUtils;
import net.rcontrol.usb.util.BytesInputStream;
import net.rcontrol.usb.util.FixedSizeQueue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.inject.Inject;

import jackpal.androidterm.emulatorview.EmulatorView;
import jackpal.androidterm.emulatorview.TermSession;

public class ControlCoordinator extends FragmentCoordinator<ControlFragment> {

    private static final String TAG = ControlCoordinator.class.getSimpleName();
    private static final String TERM_HISTORY_BUNDLE_KEY = "ControlFragment.termHistoryBundleKey";
    private static final int TERM_HISTORY_SIZE = 100;

    private final Handler mHandler = new Handler();
    private final OutputStream mOut = new ByteArrayOutputStream() {
        @Override
        public void flush() throws IOException {
            mUsbService.write(toByteArray());
            reset();
        }
    };
    private final BytesInputStream mIn = new BytesInputStream();

    private FixedSizeQueue<byte[]> mHistoryBuffer;
    private UsbService mUsbService;
    private TermSession mSession;
    private boolean mInitialized;

    @Inject
    Bus mEventBus;

    @Inject
    MainApp mApp;

    @Override
    public void register() {
        ControlFragment fragment = getRef();
        mEventBus.register(this);
        fragment.getEmulatorView().onResume();
        fragment.getInputTextView().requestFocus();
        if (mApp.isUsbServiceReady()) {
            mUsbService = mApp.getUsbService();
        }
        mInitialized = true;
    }

    @Override
    public void unregister() {
        mEventBus.unregister(this);
        mInitialized = false;
        mHandler.removeCallbacksAndMessages(null);
        getRef().getEmulatorView().onPause();
    }

    @Override
    public void init(ControlFragment fragment) {
        super.init(fragment);

        FragmentActivity activity = fragment.getActivity();
        MainApp.get(activity).getComponent().inject(this);

        fragment.setHasOptionsMenu(true);

        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

        mSession = createLocalTermSession();

        EmulatorView emulatorView = fragment.getEmulatorView();
        emulatorView.setDensity(metrics);
        emulatorView.attachSession(mSession);
        emulatorView.setTextSize((Integer) AppUtils.createAppContext(activity).get(AppUtils.FONT_SIZE));

        fragment.getSendButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendText();
            }
        });

        fragment.getInputTextView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    sendText();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void storeState(Bundle state) {
        state.putSerializable(TERM_HISTORY_BUNDLE_KEY, mHistoryBuffer);
    }

    @Override
    @SuppressWarnings("unchecked")
    public void restoreState(Bundle state) {
        if (state != null) {
            mHistoryBuffer = (FixedSizeQueue<byte[]>) state.getSerializable(TERM_HISTORY_BUNDLE_KEY);
            if (mHistoryBuffer != null && !mHistoryBuffer.isEmpty()) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                for (byte[] d : mHistoryBuffer) {
                    try {
                        bout.write(d);
                    } catch (IOException e) {
                        // ignore
                    }
                }
                mIn.put(bout.toByteArray());
                return;
            }
        }
        mHistoryBuffer = new FixedSizeQueue<>(TERM_HISTORY_SIZE);
    }

    @Subscribe
    public void handleUsbService(UsbServiceEvent e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbService");
                mUsbService = mApp.getUsbService();
            }
        });
    }

    @Subscribe
    public void handleUsbData(UsbDataEvent e) {
        final byte[] data = e.getData();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mInitialized) {
                    Log.i(TAG, "handleUsbData: " + new String(data, Charset.defaultCharset()));
                    mHistoryBuffer.add(data);
                    mIn.put(data);
                }
            }
        });
    }

    public void disconnect() {
        if (mUsbService != null) {
            mUsbService.stop();
        }
        mEventBus.post(new AppStateEvent(null));
    }

    public void cleanUp() {
        if (mSession != null) {
            mSession.finish();
        }
    }

    private void sendText() {
        if (mSession == null) {
            return;
        }
        ControlFragment fragment = getRef();
        Editable e = fragment.getInputTextView().getText();
        mSession.write(e.toString());
        mSession.write("\r\n");
        TextKeyListener.clear(e);
    }

    private TermSession createLocalTermSession() {
        TermSession session = new TermSession();
        session.setTermIn(mIn);
        session.setTermOut(mOut);
        return session;
    }
}
