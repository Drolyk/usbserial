package net.rcontrol.usb.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.usb.rcontrol.net.usbserial.BuildConfig;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public abstract class AbstractActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivityComponent();
        if (!TextUtils.equals(BuildConfig.BUILD_TYPE, "debug")) {
            Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex) {
                    Intent exInent = new Intent(AbstractActivity.this, CrashActivity.class);
                    exInent.putExtra(CrashActivity.CRASH_TRACE_DATA, asString(ex));
                    exInent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(exInent);
                    actionExit();
                }
            });
        }
    }

    protected abstract void setupActivityComponent();

    protected void actionExit() {
        finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private String asString(Throwable ex) {
        Writer writer = new StringWriter();
        ex.printStackTrace(new PrintWriter(writer, true));
        return writer.toString();
    }
}
