package net.rcontrol.usb.ui.preferences;

import android.content.Context;
import android.support.v7.preference.DialogPreference;
import android.util.AttributeSet;

public class ResetSettingsPreference extends DialogPreference {
    public ResetSettingsPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ResetSettingsPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResetSettingsPreference(Context context) {
        super(context);
    }

    public void persist() {
        persistBoolean(true);
    }
}
