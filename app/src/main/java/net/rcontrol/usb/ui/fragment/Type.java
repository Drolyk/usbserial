package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import net.rcontrol.usb.util.service.AppStateService;

import java.io.Serializable;
import java.util.Map;

public enum Type {

    INIT {
        @Override
        public Type getNext(Map<String, Serializable> ctx) {
            return SELECT;
        }

        @Override
        public Type getBack(Map<String, Serializable> ctx) {
            return null;
        }

        @Override
        public Fragment createFragment() {
            return new InitFragment();
        }
    },
    SELECT {
        @Override
        public Type getNext(Map<String, Serializable> ctx) {
            return CONTROL;
        }

        @Override
        public Type getBack(Map<String, Serializable> ctx) {
            return null;
        }

        @Override
        public Fragment createFragment() {
            return new SelectFragment();
        }
    },
    CONTROL {
        @Override
        public Type getNext(Map<String, Serializable> ctx) {
            return null;
        }

        @Override
        public Type getBack(Map<String, Serializable> ctx) {
            return null;
        }

        @Override
        public Fragment createFragment() {
            return new ControlFragment();
        }
    },
    SETTINGS {
        @Override
        public Type getNext(Map<String, Serializable> ctx) {
            return SETTINGS;
        }

        @Override
        public Type getBack(Map<String, Serializable> ctx) {
            Boolean isActive = (Boolean) ctx.get(AppStateService.USB_SERVICE_ACTIVE);
            return isActive == null || !isActive ? INIT : CONTROL;
        }

        @Override
        public Fragment createFragment() {
            return new SettingsFragment();
        }

        @Override
        public boolean isUsbAware() {
            return false;
        }
    };

    public Fragment createFragment(Map<String, Serializable> ctx) {
        Fragment fragment = createFragment();
        if (ctx != null && !ctx.isEmpty()) {
            Bundle bundle = new Bundle();
            for (Map.Entry<String, Serializable> e : ctx.entrySet()) {
                bundle.putSerializable(e.getKey(), e.getValue());
            }
            fragment.setArguments(bundle);
        }
        return fragment;
    }

    public abstract Type getNext(Map<String, Serializable> ctx);

    public abstract Type getBack(Map<String, Serializable> ctx);

    public abstract Fragment createFragment();

    public boolean isUsbAware() {
        return true;
    }
}
