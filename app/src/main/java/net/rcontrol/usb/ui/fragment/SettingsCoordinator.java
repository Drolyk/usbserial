package net.rcontrol.usb.ui.fragment;

import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;
import android.text.TextUtils;
import android.usb.rcontrol.net.usbserial.BuildConfig;
import android.usb.rcontrol.net.usbserial.R;

import com.squareup.otto.Bus;

import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.ui.preferences.InfoDialogFragment;
import net.rcontrol.usb.ui.preferences.InfoPreferences;
import net.rcontrol.usb.ui.preferences.ResetSettingsDialogFragment;
import net.rcontrol.usb.ui.preferences.ResetSettingsPreference;

import javax.inject.Inject;

public class SettingsCoordinator extends FragmentCoordinator<SettingsFragment> {

    private final SharedPreferences.OnSharedPreferenceChangeListener mResetAskListener =
            new SharedPreferences.OnSharedPreferenceChangeListener() {
                public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
                    SettingsFragment fragment = getRef();
                    String resetKey = fragment.getString(R.string.pref_reset);
                    if (TextUtils.equals(key, resetKey)) {
                        mEventBus.post(new AppStateEvent(Type.SETTINGS));
                    }
                }
            };

    @Inject
    Bus mEventBus;


    @Override
    public void init(SettingsFragment fragment) {
        super.init(fragment);
        FragmentActivity activity = fragment.getActivity();
        MainApp.get(activity).getComponent().inject(this);
        // must be retain because of bugs with savedInstance restore
        fragment.setRetainInstance(true);
    }

    @Override
    public void register() {
        SettingsFragment fragment = getRef();
        SharedPreferences prefs = android.preference.PreferenceManager.getDefaultSharedPreferences(fragment.getActivity());
        prefs.registerOnSharedPreferenceChangeListener(mResetAskListener);
    }

    @Override
    public void unregister() {
        SettingsFragment fragment = getRef();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(fragment.getActivity());
        prefs.unregisterOnSharedPreferenceChangeListener(mResetAskListener);
    }

    public void initInfoPreference() {
        SettingsFragment fragment = getRef();
        String prefInfo = fragment.getString(R.string.pref_info);
        Preference p = fragment.getPreferenceScreen().findPreference(prefInfo);
        p.setSummary(String.format(fragment.getString(R.string.info_summary),
                fragment.getString(R.string.app_name), BuildConfig.VERSION_NAME));
    }


    public DialogFragment createDialogFragment(Preference preference) {
        SettingsFragment fragment = getRef();
        if (preference instanceof ResetSettingsPreference) {
            return ResetSettingsDialogFragment.newInstance(fragment.getString(R.string.pref_reset));
        }
        if (preference instanceof InfoPreferences) {
            return InfoDialogFragment.newInstance(fragment.getString(R.string.pref_info));
        }
        return null;
    }
}
