package net.rcontrol.usb.ui.fragment;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.UsbService;
import net.rcontrol.usb.event.AppStateEvent;
import net.rcontrol.usb.event.UsbRefreshListEvent;
import net.rcontrol.usb.event.UsbServiceEvent;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public class InitCoordinator extends FragmentCoordinator<InitFragment> {

    private static final String TAG = InitCoordinator.class.getSimpleName();

    private final Handler mHandler = new Handler();
    private final Runnable mRefreshTask = new Runnable() {
        @Override
        public void run() {
            InitFragment fragment = getRef();
            if (mUsbService == null) {
                Log.d(TAG, "mRefreshTask : mUsbService == null");
                mHandler.postDelayed(mRefreshTask, UsbService.REFRESH_DELAY_TIMEOUT);
                return;
            }
            if (!mInitialized && fragment.isAdded()) {
                Log.d(TAG, "mRefreshTask : !mInitialized && isAdded()");
                mHandler.postDelayed(mRefreshTask, UsbService.REFRESH_DELAY_TIMEOUT);
                return;
            }
            initUi(mUsbService.getUsbSerialPorts());
        }
    };

    private UsbService mUsbService;
    private boolean mInitialized;

    @Inject
    Bus mEventBus;

    @Inject
    MainApp mApp;

    @Inject
    Map<String, Serializable> mAppState;

    @Override
    public void init(InitFragment fragment) {
        super.init(fragment);
        FragmentActivity activity = fragment.getActivity();
        MainApp.get(activity).getComponent().inject(this);
        fragment.getStatusTextView().setText(R.string.status_initializing);
        fragment.getProgress().setVisibility(View.VISIBLE);
        mHandler.postDelayed(mRefreshTask, UsbService.REFRESH_DELAY_TIMEOUT);
    }

    @Override
    public void register() {
        mEventBus.register(this);
        if (mApp.isUsbServiceReady()) {
            initUsbService();
        }
        mInitialized = true;
        mHandler.postDelayed(mRefreshTask, UsbService.REFRESH_DELAY_TIMEOUT * 3);
    }

    @Override
    public void unregister() {
        mInitialized = false;
        mEventBus.unregister(this);
        mHandler.removeCallbacksAndMessages(null);
    }

    @Subscribe
    public void handleUsbService(UsbServiceEvent e) {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "handleUsbService");
                initUsbService();
            }
        });
    }

    @Subscribe
    public void handleUsbUpdate(UsbRefreshListEvent e) {
        final List<UsbSerialPort> l = mUsbService.getUsbSerialPorts();
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                initUi(l);
            }
        });
    }

    private void initUi(List<UsbSerialPort> list) {
        if (mInitialized) {
            Log.i(TAG, "handleUsbRefresh: ports.size - " + list.size());
            InitFragment fragment = getRef();
            ProgressBar progressBar = fragment.getProgress();
            TextView statusTextView = fragment.getStatusTextView();
            if (!list.isEmpty()) {
                progressBar.setVisibility(View.INVISIBLE);
                statusTextView.setText(R.string.status_ready);
                mEventBus.post(new AppStateEvent(fragment.getType()));
                return;
            }
            progressBar.setVisibility(View.INVISIBLE);
            statusTextView.setText(R.string.status_no_device);
        }
    }

    private void initUsbService() {
        mUsbService = mApp.getUsbService();
        mUsbService.refreshDeviceList();
    }
}
