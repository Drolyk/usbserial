package net.rcontrol.usb.ui.preferences;

import android.content.Context;
import android.support.v7.preference.DialogPreference;
import android.usb.rcontrol.net.usbserial.R;
import android.util.AttributeSet;

public class InfoPreferences extends DialogPreference {
    public InfoPreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public InfoPreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InfoPreferences(Context context) {
        super(context);
        init();
    }

    private void init() {
        setPositiveButtonText(null);
        setNegativeButtonText(R.string.cancel);
    }
}
