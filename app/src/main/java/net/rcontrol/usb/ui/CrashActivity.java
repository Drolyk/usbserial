package net.rcontrol.usb.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.usb.rcontrol.net.usbserial.R;

public class CrashActivity extends AppCompatActivity {

    public static final String CRASH_TRACE_DATA = "net.rcontrol.usb.ui.CRASH_TRACE_DATA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash);
        Intent intent = getIntent();
        showConfirmDlg(intent.getStringExtra(CRASH_TRACE_DATA));
    }

    private void showConfirmDlg(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(msg)
                .setCancelable(false)
                .setTitle(R.string.crash_dlg_title)
                .setIcon(R.drawable.ic_warning)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        android.os.Process.killProcess(android.os.Process.myPid());
                    }
                })
                .create()
                .show();
    }
}
