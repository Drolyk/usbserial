package net.rcontrol.usb.ui.preferences;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.usb.rcontrol.net.usbserial.BuildConfig;
import android.usb.rcontrol.net.usbserial.R;
import android.view.View;
import android.widget.TextView;

public class InfoDialogFragment extends PreferenceDialogFragmentCompat {

    public static InfoDialogFragment newInstance(String key) {
        InfoDialogFragment fragment = new InfoDialogFragment();
        Bundle b = new Bundle(1);
        b.putString(ARG_KEY, key);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    protected View onCreateDialogView(Context context) {
        View view = super.onCreateDialogView(context);

        TextView infoTitleTextView = (TextView) view.findViewById(R.id.infoTitle);
        String infoString = String.format(context.getString(R.string.info_dlg_title),
                context.getString(R.string.app_name), BuildConfig.VERSION_NAME);
        infoTitleTextView.setText(Html.fromHtml(infoString));
        infoTitleTextView.setMovementMethod(LinkMovementMethod.getInstance());

        TextView infoDetailsTextView = (TextView) view.findViewById(R.id.infoDetails);
        infoDetailsTextView.setText(Html.fromHtml(context.getString(R.string.info_dlg_details)));
        infoDetailsTextView.setMovementMethod(LinkMovementMethod.getInstance());

        return view;
    }

    @Override
    public void onDialogClosed(boolean b) {
        // Empty method, nothing to save here
    }
}
