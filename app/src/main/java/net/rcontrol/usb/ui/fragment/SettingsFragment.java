package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.widget.RecyclerView;
import android.usb.rcontrol.net.usbserial.R;
import android.view.LayoutInflater;
import android.view.ViewGroup;

public class SettingsFragment extends PreferenceFragmentCompat implements TypedFragment {

    private SettingsCoordinator mCoordinator;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
        mCoordinator = new SettingsCoordinator();
        mCoordinator.init(this);
        mCoordinator.initInfoPreference();
    }

    @Override
    public RecyclerView onCreateRecyclerView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        RecyclerView recyclerView = (RecyclerView) inflater.inflate(R.layout.content_settings, parent, false);
        recyclerView.setLayoutManager(onCreateLayoutManager());
        return recyclerView;
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment f = mCoordinator.createDialogFragment(preference);
        if (f != null) {
            f.setTargetFragment(this, 0);
            f.show(this.getFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
            return;
        }
        super.onDisplayPreferenceDialog(preference);
    }

    @Override
    public void onResume() {
        super.onResume();
        setupFragmentComponent();
    }

    @Override
    public void onPause() {
        super.onPause();
        shutdownFragmentComponent();
    }

    @Override
    public Type getType() {
        return Type.SETTINGS;
    }

    @Override
    public void beforeNextType() {
    }

    @Override
    public void setupFragmentComponent() {
        mCoordinator.register();
    }

    @Override
    public void shutdownFragmentComponent() {
        mCoordinator.unregister();
    }
}
