package net.rcontrol.usb.ui.fragment;

import android.os.Bundle;
import android.util.Log;

import java.lang.ref.WeakReference;

public class FragmentCoordinator<T extends TypedFragment> {

    private static final String TAG = FragmentCoordinator.class.getSimpleName();

    protected WeakReference<T> mWeakReference;

    public void init(T t) {
        mWeakReference = new WeakReference<T>(t);
    }

    public void register() {
    }

    public void unregister() {
    }

    public void storeState(Bundle state) {
    }

    public void restoreState(Bundle state) {
    }

    protected final T getRef() {
        T target = mWeakReference.get();
        if (target == null) {
            Log.e(TAG, "weakReference: target is null.");
            throw new IllegalStateException("Illegal state. Stalled reference");
        }
        return target;
    }
}
