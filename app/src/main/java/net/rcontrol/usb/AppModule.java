package net.rcontrol.usb;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final MainApp mApp;

    public AppModule(MainApp mApp) {
        this.mApp = mApp;
    }

    @Provides
    @Singleton
    public MainApp provideApplicationContext() {
        return mApp;
    }

    @Provides
    @Singleton
    public Bus provideBus() {
        return new Bus(ThreadEnforcer.ANY);
    }

    @Provides
    @Singleton
    public Map<String, Serializable> provideAppState() {
        return new ConcurrentHashMap<>();
    }
}
