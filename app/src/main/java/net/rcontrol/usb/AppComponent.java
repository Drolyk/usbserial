package net.rcontrol.usb;

import net.rcontrol.usb.ui.MainCoordinator;
import net.rcontrol.usb.ui.fragment.ControlCoordinator;
import net.rcontrol.usb.ui.fragment.InitCoordinator;
import net.rcontrol.usb.ui.fragment.SelectCoordinator;
import net.rcontrol.usb.ui.fragment.SettingsCoordinator;
import net.rcontrol.usb.util.service.AppStateService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(
        modules = {AppModule.class}
)
public interface AppComponent {

    void inject(UsbService service);

    void inject(MainApp app);

    // utility service
    void inject(AppStateService service);

    // coordinators
    void inject(MainCoordinator coordinator);

    void inject(ControlCoordinator coordinator);

    void inject(InitCoordinator coordinator);

    void inject(SelectCoordinator coordinator);

    void inject(SettingsCoordinator coordinator);
}
