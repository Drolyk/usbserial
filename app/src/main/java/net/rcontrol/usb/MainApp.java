package net.rcontrol.usb;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.squareup.otto.Bus;

import net.rcontrol.usb.event.UsbServiceEvent;

import javax.inject.Inject;

public class MainApp extends Application {

    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            UsbService.LocalBinder binder = (UsbService.LocalBinder) service;
            mUsbService = binder.get();
            mUsbServiceReady = true;
            mEventBus.post(new UsbServiceEvent(true));
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mUsbServiceReady = false;
            mEventBus.post(new UsbServiceEvent(false));
        }
    };

    private UsbService mUsbService;
    private boolean mUsbServiceReady;
    private AppComponent mComponent;

    @Inject
    Bus mEventBus;

    public static MainApp get(Context context) {
        return (MainApp) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        bindService(new Intent(this, UsbService.class), mServiceConnection, BIND_AUTO_CREATE);
        initAppComponent();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        unbindService(mServiceConnection);
    }

    public AppComponent getComponent() {
        return mComponent;
    }

    public UsbService getUsbService() {
        if (!mUsbServiceReady) {
            throw new RuntimeException("UsbService unbound");
        }
        return mUsbService;
    }

    public boolean isUsbServiceReady() {
        return mUsbServiceReady;
    }

    private void initAppComponent() {
        mComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        mComponent.inject(this);
    }
}
