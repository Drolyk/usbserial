package net.rcontrol.usb;

import android.util.Log;

import java.lang.ref.WeakReference;

public class Coordinator<T> {
    private static final String TAG = Coordinator.class.getSimpleName();

    protected WeakReference<T> mWeakReference;

    public void init(T t) {
        mWeakReference = new WeakReference<T>(t);
    }

    public void register() {
    }

    public void unregister() {
    }

    protected final T getRef() {
        T target = mWeakReference.get();
        if (target == null) {
            Log.e(TAG, "weakReference: target is null.");
            throw new IllegalStateException("Illegal state. Stalled reference");
        }
        return target;
    }
}
