package net.rcontrol.usb.event;

import net.rcontrol.usb.ui.fragment.Type;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

public class AppStateEvent {
    private final Type mType;
    private final Map<String, Serializable> mCtx;

    public AppStateEvent(Type type) {
        this(type, Collections.<String, Serializable>emptyMap());
    }

    public AppStateEvent(Type mType, Map<String, Serializable> mCtx) {
        this.mType = mType;
        this.mCtx = mCtx;
    }

    public Type getType() {
        return mType;
    }

    public Map<String, Serializable> getCtx() {
        return mCtx;
    }
}
