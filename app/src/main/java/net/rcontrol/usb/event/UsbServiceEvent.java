package net.rcontrol.usb.event;

public class UsbServiceEvent {
    private final boolean mBound;

    public UsbServiceEvent(boolean mBound) {
        this.mBound = mBound;
    }

    public boolean isBound() {
        return mBound;
    }
}
