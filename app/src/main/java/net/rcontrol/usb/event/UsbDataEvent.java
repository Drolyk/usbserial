package net.rcontrol.usb.event;

import java.util.Arrays;

public class UsbDataEvent {
    private final byte[] mData;

    public UsbDataEvent(byte[] data) {
        this.mData = Arrays.copyOf(data, data.length);
    }

    public byte[] getData() {
        return Arrays.copyOf(mData, mData.length);
    }
}
