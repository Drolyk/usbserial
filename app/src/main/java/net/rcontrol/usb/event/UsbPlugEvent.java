package net.rcontrol.usb.event;

import android.hardware.usb.UsbDevice;

public class UsbPlugEvent {
    private final UsbDevice mUsbDevice;

    private final boolean mAttached;

    public UsbPlugEvent(UsbDevice usbDevice, boolean attached) {
        this.mUsbDevice = usbDevice;
        this.mAttached = attached;
    }


    public UsbDevice getUsbDevice() {
        return mUsbDevice;
    }

    public boolean isAttached() {
        return mAttached;
    }
}
