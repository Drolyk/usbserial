package net.rcontrol.usb.event;

public class UsbErrorEvent {

    private final String mErrorMessage;

    public UsbErrorEvent(String errorMessage) {
        this.mErrorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}
