package net.rcontrol.usb.util;

import java.util.Map;

public interface AppContext extends Map<String, Object> {
    String FRAGMENT_STATE_KEY = "fragmentStateKey";
}
