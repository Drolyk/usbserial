package net.rcontrol.usb.util;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class FixedSizeQueue<T> extends AbstractQueue<T> implements Serializable {

    private static final long serialVersionUID = 876323262645576154L;
    private final int mSize;
    private final List<T> mList = new LinkedList<>();

    public FixedSizeQueue(int size) {
        this.mSize = size;
    }

    @NonNull
    @Override
    public Iterator<T> iterator() {
        return mList.iterator();
    }

    @Override
    public int size() {
        return mList.size();
    }

    @Override
    public boolean offer(T t) {
        if (mList.size() == mSize) {
            mList.remove(0);
        }
        return mList.add(t);
    }

    @Override
    public T poll() {
        return mList.isEmpty() ? null : mList.remove(0);
    }

    @Override
    public T peek() {
        return mList.isEmpty() ? null : mList.get(0);
    }
}
