package net.rcontrol.usb.util;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BytesInputStream extends InputStream {

    private static final String TAG = BytesInputStream.class.getSimpleName();

    private final Lock lock = new ReentrantLock();
    private final Condition newData = lock.newCondition();

    private boolean available;
    private List<Byte> bytes = new LinkedList<>();

    @Override
    public int read() throws IOException {
        lock.lock();
        byte b = -1;
        try {
            while (!available) {
                try {
                    newData.await();
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    Log.e(TAG, "read() : Interrupted");
                    b = -1;
                }
            }
            available = !bytes.isEmpty();
            if (available) {
                b = bytes.remove(0);
            }
            newData.signalAll();
        } finally {
            lock.unlock();
        }
        return b;
    }

    public void put(byte[] data) {
        lock.lock();
        try {
            for (byte b : data) {
                bytes.add(b);
            }
            available = !bytes.isEmpty();
            newData.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
