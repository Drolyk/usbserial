package net.rcontrol.usb.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.preference.PreferenceManager;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();

    // conn settings
    public static final String BAUD_RATE = "baudRate";
    public static final String DATA_BITS = "dataBits";
    public static final String STOP_BITS = "stopBits";
    public static final String PARITY = "parity";
    // app settings
    public static final String FONT_SIZE = "fontSize";

    public static void closeQuietly(InputStream input) {
        closeQuietly((Closeable) input);
    }

    public static void closeQuietly(OutputStream output) {
        closeQuietly((Closeable) output);
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException ioe) {
                Log.e(TAG, "IOException: " + ioe.getMessage(), ioe);
            }
        }
    }

    public static Map<String, Integer> createSerialContext(Context context) {
        Map<String, Integer> configCtx = new HashMap<>();
        configCtx.put(BAUD_RATE, createInteger(context, R.string.pref_baud_rate, R.string.pref_baudRate_default));
        configCtx.put(DATA_BITS, createInteger(context, R.string.pref_data_bits, R.string.pref_dataBits_default));
        configCtx.put(STOP_BITS, createInteger(context, R.string.pref_stop_bits, R.string.pref_stopBits_default));
        configCtx.put(PARITY, createInteger(context, R.string.pref_parity, R.string.pref_parity_default));
        return configCtx;
    }

    public static Map<String, Serializable> createAppContext(Context context) {
        Map<String, Serializable> appCtx = new HashMap<>();
        appCtx.put(FONT_SIZE, createInteger(context, R.string.pref_font_size, R.string.pref_font_size_default));
        return appCtx;
    }

    private static Integer createInteger(Context ctx, int key, int defaultKey) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ctx);
        String stringKey = ctx.getString(key);
        String stringDefault = ctx.getString(defaultKey);
        return Integer.parseInt(pref.getString(stringKey, stringDefault));
    }
}
