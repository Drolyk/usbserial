package net.rcontrol.usb.util.service;

import android.content.Context;

import net.rcontrol.usb.ui.fragment.AbstractFragment;

import java.lang.ref.WeakReference;

public class AbstractUtilityService {
    protected WeakReference<Context> mWeakContext;
    private boolean initialized = false;

    public void register(Context context) {
        if (initialized) {
            throw new IllegalStateException("Context already bound to service");
        }
        registerImpl(context);
    }

    public void register(AbstractFragment fragment) {
        if (initialized) {
            throw new IllegalStateException("Context already bound to service");
        }
        registerImpl(fragment.getActivity());
    }

    protected void init(Context context) {
    }

    private void registerImpl(Context context) {
        mWeakContext = new WeakReference<Context>(context);
        initialized = true;
        init(context);
    }
}
