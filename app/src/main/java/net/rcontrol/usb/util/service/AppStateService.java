package net.rcontrol.usb.util.service;

import android.content.Context;

import net.rcontrol.usb.MainApp;
import net.rcontrol.usb.ui.fragment.Type;

import java.io.Serializable;
import java.util.Map;

import javax.inject.Inject;

public class AppStateService extends AbstractUtilityService {
    public static final String FRAGMENT_STATE_KEY = "fragmentStateKey";
    public static final String USB_SERVICE_ACTIVE = "usbServiceActive";
    public static final String EXTRA_TITLE_KEY = "extraTitleKey";

    @Inject
    Map<String, Serializable> mAppState;

    public Type getFragmentType() {
        Type fmType = (Type) mAppState.get(FRAGMENT_STATE_KEY);
        return fmType == null ? Type.INIT : fmType;
    }

    public void setFragmentType(Type type) {
        mAppState.put(FRAGMENT_STATE_KEY, type);
    }

    public String getExtraTitle() {
        return mAppState.containsKey(EXTRA_TITLE_KEY) ? (String) mAppState.get(EXTRA_TITLE_KEY) : "";
    }

    public void setExtraTitle(String text) {
        mAppState.put(EXTRA_TITLE_KEY, text == null ? "" : text);
    }

    @Override
    protected void init(Context context) {
        MainApp.get(context).getComponent().inject(this);
    }
}
