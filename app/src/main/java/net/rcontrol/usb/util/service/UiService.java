package net.rcontrol.usb.util.service;

import android.content.Context;
import android.usb.rcontrol.net.usbserial.R;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class UiService extends AbstractUtilityService {
    private static final String TAG = UiService.class.getSimpleName();

    public void createToast(ToastState toastState, int stringId) {
        Context context = mWeakContext.get();
        if (context == null) {
            Log.e(TAG, "context is obsolete");
            return;
        }
        createToast(toastState, context.getString(stringId));
    }

    public void createToast(ToastState toastState, String msg) {
        Context context = mWeakContext.get();
        if (context == null) {
            Log.e(TAG, "context is obsolete");
            return;
        }
        Toast toast = new Toast(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View toastRoot = inflater.inflate(R.layout.toast, null);
        ImageView img = (ImageView) toastRoot.findViewById(R.id.toastIcon);
        TextView text = (TextView) toastRoot.findViewById(R.id.toastMessage);
        img.setImageResource(toastState.getId());
        text.setText(msg);
        toast.setView(toastRoot);
        toast.setGravity(Gravity.BOTTOM, 0, (int) context.getResources().getDimension(R.dimen.toast_offset));
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    public enum ToastState {
        OK(R.drawable.ic_info),
        WARNING(R.drawable.ic_warning),
        ERROR(R.drawable.ic_error);

        private final int id;

        ToastState(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }
}
